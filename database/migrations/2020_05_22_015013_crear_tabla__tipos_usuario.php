<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CrearTablaTiposUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        ///////////////////////////////////////////////////////////////////////////////////////////////
        // TABLA QUE CONTIENE LOS TIPOS DE USUARIOS DEL SISTEMA (Persona fisica, juridica, empleado) //
        ///////////////////////////////////////////////////////////////////////////////////////////////
        Schema::create('TiposUsuario', function (Blueprint $table) {
            $table->tinyIncrements("id");
            $table->string("tipoUsuario",30);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('TiposUsuario');
    }
}
