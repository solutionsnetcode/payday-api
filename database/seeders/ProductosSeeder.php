<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $productos = array(
            array(
                'nombre' => 'Notebook ACER A3',
                'descripcion' => 'Una notebook acer I5 con 8GB de ram',
                'costo'  => 250,
                'stock'  => 5
            ),
            array(
                'nombre' => 'Celular Xiaomi',
                'descripcion' => 'Celular cuad-core con 6GB de ram. Pantalla 5 pulgadas',
                'costo'  => 150,
                'stock'  => 2
            ),
            array(
                'nombre' => 'Campera Adidas',
                'descripcion' => 'Campera marca adidas de color azul',
                'costo'  => 100,
                'stock'  => 10
            ),
            array(
                'nombre' => 'Teclado y Mouse',
                'descripcion' => 'Teclado y mouse inalambricos marca HP',
                'costo'  => 95,
                'stock'  => 25
            )
        );

        foreach ($productos as $key => $producto) {
            DB::table('ProductosCatalogo')->insert([
                'nombre' => $producto['nombre'],
                'descripcion' => $producto['descripcion'],
                'costo' => $producto['costo'],
                'stock' => $producto['stock']
            ]);
        }
    }
}
