<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/** 
  * Esta clase es la utilizada para utenticarse en el sistema y es la que contiene toda la inforamcion del usuario.
  * @author NetCode Solutions solutionsnetcode@gmail.com
*/
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;
    
    protected $table = 'Usuarios';
    protected $primaryKey = 'id';
    protected $remember_token = true;
    protected $guarded = ['id'];

    protected $fillable = [
        'tipo', 'email', 'contrasena', 'fechaRegistro','tokenActivacion', 'rememberToken', 'puntos'
    ];

    protected $hidden = [
        'password', 'contrasena', 'deleted_at', 'updated_at', 'remember_token',
    ];

    public function getAuthPassword()
    {
        $pass = $this->contrasena;
        return $pass;
    }


    public function datos(){
        return $this->hasOne(PersonaFisica::class, 'idUsuario' ,'id');
    }

    public function servicios(){
        return $this->belongsToMany(Servicio::class, 'ServiciosAgendados', 'idUsuario', 'idServicio');
    }

    // public function datos(){
    //     if ($this->IdTipoUsuario == Constantes::PersonaFisica){
    //         return $this->hasOne(PersonaFisica::class, 'IdUsuario' ,'IdUsuario');
    //     }
    //     elseif($this->IdTipoUsuario == Constantes::PersonaJuridica){
    //         return $this->hasOne(PersonaJuridica::class, 'IdUsuario' ,'IdUsuario');
    //     }  
    //     else{
    //         return $this->hasOne(PersonaEmpleado::class, 'IdUsuario' ,'IdUsuario');
    //     }   
    // }
}
