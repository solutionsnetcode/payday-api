<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersonaEmpleado extends Model
{
    protected $table = 'PersonasEmpleados';
    protected $primaryKey = 'IdEmpleado';

    protected $fillable = [
        'IdUsuario', 'Nombre', 'Apellido', 'Documento', 'Sexo', 'FechaNacimiento', 'IdRol'
    ];

    public function Rol(){
        return $this->hasOne(Rol::class, 'IdRol' ,'IdRol');
    }
}
