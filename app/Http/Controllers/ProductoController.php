<?php

namespace App\Http\Controllers;

use App\Models\ProductoCatalogo;
use Exception;
use Illuminate\Http\Request;

class ProductoController extends Controller
{
    public function listarProductos($productoId = null){
        try{
            if ($productoId){
                $producto = ProductoCatalogo::findOrFail($productoId);
                return response()->json(['respuesta' => 'Producto de catalogo obtenido correctamente', 'producto' => $producto], 200);
            }else{
                return response()->json(['respuesta' => 'Productos obtenidos correctamente', 'productos' => ProductoCatalogo::where('deleted_at',null)->get()], 200);
            }        
        }catch(Exception $e){
            return response()->json(['respuesta' => 'No se pudieron obtener los productos. Contacte a soporte'], 500);
        }
    }
}
